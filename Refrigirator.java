public class Refrigirator
{
    private String brand;
    private int contents;
    private int maxCapacity;
	
	public Refrigirator(String brand, int contents, int maxCapacity)
	{
		this.brand = brand;
		this.contents = contents;
		this.maxCapacity = maxCapacity;
	}
    public void increaseContents()
    {
        if(this.contents < this.maxCapacity)
        {
            this.contents++;
        }
    }
    public void decreaseContents()
    {
        if(this.contents != 0)
        {
            this.contents--;
        }
    }
	public boolean changeBrand(String brand)
	{
		if(validate(brand))
		{
			this.brand = brand;
			return true;
		}
		return false;
	}
	private boolean validate(String input)
	{
		return (input.equals("LG") || input.equals("GE") || input.equals("SAMSUNG"));
	}
	//Getters
	public String getBrand()
	{
		return this.brand;
	}
	public int getContents()
	{
		return this.contents;
	}
	public int getMaxCapacity()
	{
		return this.maxCapacity;
	}
	//Setters
	public void setMaxCapacity(int maxCapacity)
	{
		if(maxCapacity > 0)
		{
			this.maxCapacity = maxCapacity;
		}
	}
	public void setContents(int contents)
	{
		if(contents>0 && contents <= maxCapacity)
		this.contents = contents;
	}
}
