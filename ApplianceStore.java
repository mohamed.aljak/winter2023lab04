import java.util.Scanner;
public class ApplianceStore
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        Refrigirator[] refrigirators = new Refrigirator[2];
		System.out.println("Please provide input:");
        for(int i=0;i<2;i++)
        {
             System.out.println("\n" + "Brand name");
             String brand = scanner.next();
             System.out.println("Max capacity");
             int maxCapacity = scanner.nextInt();
			 scanner.nextLine();
             System.out.println("Contents");
             int contents = scanner.nextInt();
			 scanner.nextLine();
			 refrigirators[i] = new Refrigirator(brand, contents, maxCapacity);
        }
		
		//printing before changing values
		System.out.println("\n" + "Printing: Last");
        System.out.println(refrigirators[1].getBrand());
        System.out.println(refrigirators[1].getMaxCapacity());
        System.out.println(refrigirators[1].getContents());
		
		System.out.println("\n" + "What name to change the brand name to:" );
		String newName = scanner.next();
		while(!refrigirators[1].changeBrand(newName))
		{
			System.out.println("\n" + "Error: What name to change the brand name to:" );
			newName = scanner.next();
		}
		
		//changing values
		System.out.println("\n" + "Rename last refrigirator's maxCapacity");
		refrigirators[1].setMaxCapacity(scanner.nextInt());
		System.out.println("Change last refrigirator's contents");
		refrigirators[1].setContents(scanner.nextInt());
		scanner.nextLine();
		
		//printing after changing values
		System.out.println("\n" + "Printing: Last");
        System.out.println(refrigirators[1].getBrand());
        System.out.println(refrigirators[1].getMaxCapacity());
        System.out.println(refrigirators[1].getContents());
    }
}